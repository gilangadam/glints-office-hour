//buat 4 fungsi sederhana untuk menghitung penjumlahan, pengurangan,
// perkalian, dan pembagian dua buah angka.
//contoh:
//input -> (2 ,2) , (4-2), (10,2), (50,10)
//output -> (4),  (2), (20), (5)

function Plus(number1, number2) {
  //tulis kode dibawah ini
  return(number1 + number2);
}

function Minus(n1, n2) {
  //tulis kode dibawah ini
  return(n1 - n2);
}

function Times(int1, int2) {
  //tulis kode dibawah ini
  return(int1 * int2);
}

function Divide(num, nums) {
  //tulis kode dibawah ini
  return(num / nums);
}

//kode dibawah ini jangan diedit
function Test(fun, result, preview) {
  console.log(fun === result, preview);
}

Test(Plus(1, 2), 3, `result ${Plus(1, 2)}`);
Test(Plus(2, -4), -2, `result ${Plus(2, -4)}`);
Test(Minus(10, 6), 4, `result ${Minus(10, 6)}`);
Test(Divide(100, 4), 25, `result ${Divide(100,4)}`);
Test(Times(6, 7), 42, `result ${Times(6, 7)}`);

