const queue = (arr) => {
  //tuliskan kode disini
  let text = "";
  // if(arr[0] === "you") {
  //   text = `anda berada di antrian paling depan`;
  // } else if (arr[arr.length - 1] === "you") {
  //   text = `anda berada di antrian paling belakang`;
  // } else {
  //   let you = (a) => a === "you";
  //   text = `anda berada di antrian nomor ${arr.findIndex(you) + 1}`;
  // }
  arr[0] === you
    ? text = `anda berada di antrian paling depan`
    : arr[arr.length - 1] === you 
      ? text = `anda berada di antrian paling belakang`
      : text = `anda berada di antrian nomor ${arr.findIndex(person => person === you) + 1}`;  
  return text;
};

var strangers = "strangers";
var you = "you";

// kode dibawah jangan diedit
function Test(fun, result) {
  console.log(fun === result, fun);
}

Test(
  queue([
    strangers,
    strangers,
    strangers,
    you,
    strangers,
    strangers,
    strangers,
    strangers,
  ]),
  "anda berada di antrian nomor 4"
);
Test(
  queue([you, strangers, strangers, strangers, strangers]),
  "anda berada di antrian paling depan"
);
Test(
  queue([strangers, strangers, , strangers, strangers, strangers, you]),
  "anda berada di antrian paling belakang"
);

Test(
  queue([
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    strangers,
    you,
    strangers,
    strangers,
  ]),
  "anda berada di antrian nomor 14"
);
