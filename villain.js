// Terdapat array yang berisi tokoh antagonist dari manga Jojo's Bizzare Adventure.
// Gunakan looping untuk memunculkan semua tokoh-tokoh tersebut di dalam console.log sesuai dengan part mereka
// contoh
// Input -> Antagonist()
// Output ->
// Part 1 antagonist is Dio Brando
// Part 2 antagonist is Kars
// Part 3 antagonist is DIO
// Part 4 antagonist is Kira Yoshikage
// Part 5 antagonist is Diavolo
// Part 6 antagonist is Enrico Pucci
// Part 7 antagonist is Funny Valentine
// Part 8 antagonist is Akefu Satoru

const villain = [
  "Dio Brando",
  "Kars",
  "DIO",
  "Kira Yoshikage",
  "Diavolo",
  "Enrico Pucci",
  "Funny Valentine",
  "Akefu Satoru",
];

let i = 0;
function Antagonist() {
  //tulis kode disini
  for (i; i < villain.length; i++) {
    console.log(`Part ${i+1} antagonist is ${villain[i]}`);
  }
}

Antagonist();
