// terdata class Hero dan sub-class Sidekick yang perlu di debug.
// Class Hero menerima 3 parameter berupa name, city, dan ability,
// sedangkan sub-class Sidekick menerima 5 parameter. 3 dari parentnya dan 2 lagi adalah age dan hero
// buatlah minimal 3 objek dari class Hero dan 3 objek dari sub-class Sidekick.
// 6 objek diatas harus memiliki fungsi bernama getDetail yan nantinya akan mereturn values dari parameter.
// Return dari class Hero adalah nama, kota asal, dan kemampuan.
// Return dari sub-class Sidekick adalah nama, umur, kota asal, nama pahlawan yang mereka dampingi, dan kemampuan.
// Input -> console.log(Batman.getDetail())
// Output -> "I am Batman. I am from Gotham and I can beat people up with gadgets"
// Input -> console.log(Robin.getDetail())
// Output -> "I am Robin. I am 12 years old and I come from Gotham. I can help Batman with acrobatics"

class Hero {
  constructor(name, city, ability) {
    this.name = name;
    this.city = city;
    this.ability = ability;
  }
  set setName(value) {
    if (typeof value === "string") {
      this.name = value;
    } else {
      this.name = null;
    }
  }
  set setCity(value) {
    if (typeof value === "string") {
      this.city = value;
    } else {
      this.city = null;
    }
  }
  set setAbility(value) {
    if (typeof value === "string") {
      this.hero = value;
    } else {
      this.ability = null;
    }
  }
  get getName() {
    if (this.name  === null) {
      return `value belum di set`;
    }
    return this.name;
  }
  get getCity() {
    if (this.city === null) {
      return `value belum di set`;
    }
    return this.city;
  }
  get getAbilty() {
    if (this.ability === null) {
      return `value belum di set`;
    }
    return this.ability;
  }
  getDetail() {
      return (`I am ${this.name}. I am from ${this.city} and I can beat people up with ${this.ability}!`);
  }
}

class Sidekick extends Hero {
  constructor(hero, age, city, name, ability) {
    super(name, city, ability);
    this.age = age;
    this.hero = hero;
  }
  set setaAge(value) {
    if (typeof value === "number") {
      this.age = value;
    } else {
      this.age = null;
    }
  }
  set setHero(value) {
    if (typeof value === "string") {
      this.name = value;
    } else {
      this.hero = null;
    }
  }
  get getAge() {
    if (this.age === null) {
      return `value belum di set`;
    }
    return this.age;
  }
  get getHero() {
    if (this.hero === null) {
      return `value belum di set`;
    }
    return this.hero;
  }
  getDetail() {
    return `I am ${this.hero}. I am ${this.age} years old and I come from ${this.city}. I can help ${this.name} with ${this.ability}!`;
  }
}


let Thor = new Hero("Thor", "Asgardian", "Mjollnir");
let Gundala = new Hero("Gundala", "Jakarta", "Petir");
let Aquaman = new Hero("Aquaman", "Atlantis", "Trisula");

let WonderWoman = new Sidekick("Wonder Woman", 35, "Themyscira", "Superman", "Lasso");
let Hulk = new Sidekick("Hulk", 40, "Dayton", "Iron Man", "Ultra Strength");
let Abe = new Sidekick("Abe Sapien", 60, " Washington, D.C", "Hellboy", "Psychic Ability");


//buatlah minimal 3 objek dari class Hero dan 3 objek dari sub-class Sidekick.
// 6 objek diatas harus memiliki fungsi bernama getDetail yan nantinya akan mereturn values dari parameter.
// Return dari class Hero adalah nama, kota asal, dan kemampuan.
// Return dari sub-class Sidekick adalah nama, umur, kota asal, nama pahlawan yang mereka dampingi, dan kemampuan.

console.log(Thor.getDetail());
console.log(Gundala.getDetail());
console.log(Aquaman.getDetail());
console.log(WonderWoman.getDetail());
console.log(Hulk.getDetail());
console.log(Abe.getDetail());