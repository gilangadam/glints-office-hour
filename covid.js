// buatlah sebuah fungsi yang mereturn saran saat masa-masa covid.
// fungsi ini memiliki tiga parameter.
// return dari fungsi ini adalah tiga buah variable yang
// berisi pesan yang bergantung pada tiga parameter diatas.
// contoh
// Input -> isAlive = true, isPandemic = true, isVaccine = false
// Output -> "Gunakan masker saat keluar rumah, cuci tangan setiap hari, dan jaga jarak minimal 2 meter dengan orang lain"

//pesan jangan diedit
const isAliveFalse = "RIP";
const isPandemicTrue =
  "Gunakan masker saat keluar rumah, cuci tangan secara berkala, dan jaga jarak dengan orang lain minimal 2 meter";
const isVaccinneTrue = "Segera daftarkan diri anda untuk vaksinasi";

function Covid(isAlive, isPandemic, isVaccinne) {
  // tulis code dibawah ini
  if (isAlive === false) {
    return isAliveFalse;
  } else if (isAlive === true && isVaccinne == false) {
    return isPandemicTrue;
  } else {
    return isVaccinneTrue;
  }
}

//kode dibawah ini jangan diedit
function Test(fun, result) {
  console.log(fun === result, fun);
}
Test(Covid(false, true, true), isAliveFalse);
Test(Covid(true, false, false), isPandemicTrue);
Test(Covid(true, true, false), isPandemicTrue);
Test(Covid(true, true, true), isVaccinneTrue);
Test(Covid(true, false, true), isVaccinneTrue);
