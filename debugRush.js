// terdapat fungsi bernama Mod yang memiliki 2 parameter
// return dari fungsi ini adalah modulus dari 2 parameter.

function Mod(mod1, mod2){
    return mod1 % mod2 === 0
}

console.log(Mod(50,2));

// terdapat fungsi bernama Hangout yang memiliki 2 parameter yaitu rain dan money.
// return dari fungsi ini sesuai dari kondisi dari parameter.
// Input -> rain = true money = true, rain = false, money = false, rain = false, money = true, rain = true money = false
// Output ->"Let's wait until the rain goes away", "Sorry, but I don't have enough money to hangout", "Let's go hangout", "Maybe next time, guys"


const Hangout = (rain,money) => {
    if(rain === true && money === true){
        return `Let's wait until the rain goes away`;
    }
    else if (rain === false && money === false) {
        return `Sorry, but I don't have enough money to hangout`;
    }
    else if (rain === false && money === true){
        return "Let's go hangout";
    } else {
        return "Maybe next time, guys";
    }
}

console.log(Hangout(true,true));
console.log(Hangout(true,false));
console.log(Hangout(false,true));
console.log(Hangout(false,false));

// terdapat sebuah array bernama employees dan fungsi bernama Salary yang menerima 1 parameter.
// Return dari fungsi tersebut adalah urutan gaji karyawan yang diterima dari parameter
// Input -> Salary(1)
// Output -> Subaru Natsuki, gaji 50 Juta

const employees = [
    {
        name: "Subaru Natsuki",
        salary: "50 juta"
    },
    {
        name: "Reinhardt van Astrea",
        salary: "30 juta"
    },
    {
        name: "Rem",
        salary: "10 juta"
    }
]

function Salary(num){
    return `${employees[num-1].name}, gaji ${employees[num-1].salary}`
}

console.log(Salary(3));

// terdapat sebuah fungsi bernama Criminals tidak mempunyai parameter dan 2 buah array bernama surName dan givenName
// return dari fungsi ini adalah nama depan dan nama belakang yang berasal dari 2 buah array tersebut
// Input -> Criminals()
// return -> "Claude Speed, Tommy Vercetti, Carl Johnson, Toni Cipriani, Victor Vance, Niko Bellic, Johnny Klebitz, Luiz Lopez, Huang Lee, Michael De Santa, Franklin Clinton, Trevor Philips, YOU"

surName = [
    "Speed",
    "Vercetti",
    "Johnson",
    "Cipriani",
    "Vance",
    "Bellic",
    "Klebitz",
    "Lopez",
    "Lee",
    "De Santa",
    "Clinton",
    "Philips",
    " "
]

givenName = [
    "Claude",
    "Vercetti",
    "Carl",
    "Toni",
    "Victor",
    "Niko",
    "Johnny",
    "Luiz",
    "Huang",
    "Michael",
    "Franklin",
    "Trevor",
    "YOU"
]

const Criminals = ()=> {
    for(let i = 0; i<surName.length; i++){
        console.log(`${givenName[i]} ${surName[i]}`);
    }
}

Criminals();

// teerdapat fungsi bernama Rangking yang tidak menerima parameter dan array berisi nama siswa
// return dari fungsi ini adalah siswa dengan ranking terakhir dengan nilai mereka serta siswa dengan nilai terendah serta nilai mereka
// Input -> Rangking()
// Output -> "The first ranked student is Arthur with 100 and the last ranked student is Micah with 10"

const students = [
    {
        name: "Arthur",
        grade: 100
    },
    {
        name: "John",
        grade: 70
    },
    {
        name: "Micah",
        grade: 10
    },
    {
        name: "Dutch",
        grade: 60
    },
    {
        name: "Jack",
        grade: 50
    }
]

function Rangking() {
  var newObj = students.sort(function (a, b) {return a.grade - b.grade});
  
  var min = newObj[0];
  var max = newObj[newObj.length - 1];
    return `The first ranked student is ${max.name} with ${max.grade} and the last ranked student is ${min.name} with ${min.grade}`;
}
console.log(Rangking());

// terdapat class bernama Donkol yang memiliki 2 parameter berupa jarak dan ongkir
// buatlah sebuah objek dari class ini dan console.log getGetail yang returnnya berupa jarak * ongkir + 5000

class Donkol{
    constructor(jarak, ongkir) {
    this.jarak = jarak;
    this.ongkir = ongkir;
    }
    getDetail(){
        return this.jarak * this.ongkir + 5000;
    }
}

let donkol = new Donkol(32,10);

console.log(donkol.getDetail());

