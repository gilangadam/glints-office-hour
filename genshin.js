// terdapat sebuah array bernama characters berisikan karakter dari Genshin Impact.
// buatlah sebuah fungsi yang mereturn jumlah karakter, karakter dengan level tertinggi, dan karakter dengan level terendah.
// Input -> genshin()
// Output -> "I have 5 characters. My most played character is Razor level 94 and my least played character is Amber level 7."

const characters = [
  {
    name: "Aether",
    level: 24,
  },
  {
    name: "Klee",
    level: 12,
  },
  {
    name: "Razor",
    level: 94,
  },
  {
    name: "Amber",
    level: 7,
  },
  {
    name: "Barbara",
    level: 44,
  },
];

const genshin = (chars) => {
  var newObj = chars.sort(function (a, b) {return a.level - b.level});
  //console.log(newObj);
  var min = newObj[0];
  var max = newObj[newObj.length - 1];
  return `I have ${chars.length} characters. My most played character is ${max.name} level ${max.level} and my least played character is ${min.name} level ${min.level}.`;
};

function Test(fun, result) {
  console.log(fun === result, fun);
}

Test(
  genshin(characters),
  "I have 5 characters. My most played character is Razor level 94 and my least played character is Amber level 7."
);
