// Terdapat sebuah fungsi bernama cartTotal yang perlu didebug
// fungsi ini menerima parameter berupa id yang nanti akan
// mereturn nama member dan isi keranjang belanjaan mereka
// hint: pastikan tak ada typo, cek semua isi variable, dan lihat return dari console untuk mengecek errornya dimana
// Input -> cartTotal(123)
// Output -> "Barang belanjaan M. Avdol adalah Susu Kambing, Telur Bebek, dan Tepung Kanji "

const shopping = [
  {
    name: "M. Avdol",
    status: "member",
    id: 123,
    cart: [
      {
        id: 1,
        name: "Susu Kambing",
        qty: 3,
        price: 25000,
      },
      {
        id: 2,
        name: "Telur Bebek",
        qty: 10,
        price: 30000,
      },
      {
        id: 3,
        name: "Tepung Kanji",
        qty: 5,
        price: 10000,
      },
    ],
  },
  {
    name: "Kakyoin",
    status: "non-member",
    id: 010,
    cart: [
      {
        id: 1,
        name: "Tupperware",
        qty: 1,
        price: 250000,
      },
      {
        id: 2,
        name: "Jam Tangan",
        qty: 2,
        price: 300000,
      },
    ],
  },
  {
    name: "J.P. Polnareff",
    status: "member",
    id: 1234,
    cart: [
      {
        id: 1,
        name: "Bunga Tulip Plastik",
        qty: 5,
        price: 3000,
      },
      {
        id: 2,
        name: "Makanan Kura-kura",
        qty: 8,
        price: 500000,
      },
      {
        id: 3,
        name: "Makanan Anjing",
        qty: 10,
        price: 200000,
      },
      {
        id: 4,
        name: "Permen Karet",
        qty: 7,
        price: 15000,
      },
      {
        id: 5,
        name: "Kaos Kaki Ajaib",
        qty: 1,
        price: 99999,
      },
    ],
  },
];

// const cartTotal = (id) => {
//   let filters = shopping.filter((shop) => shop.id === id);
//   let cartName = filters.map((carts) => carts.cart.name);
//   // console.log(
//   //   `Barang belanjaan ${filters[0].name} adalah ${cartName.slice(0, -1).join(", ")}${
//   //     cartName.length > 2 ? `, dan ` : ` dan `
//   //   }${cartName.pop()} `
//   // );
//   console.log(filters);
//   console.log(filters[0].cart);
// };

const cartTotal = (id) => {
  let find = shopping.find((shop) => shop.id === id);
  let cartName = find.cart.map((carts) => carts.name);
  return (
    `Barang belanjaan ${find.name} adalah ${cartName
      .slice(0, -1)
      .join(", ")}${cartName.length > 2 
        ? `, dan ` 
        : ` dan `}${cartName.pop()}`
  );
};


//console.log(cartTotal(123));

function Test(fun, result) {
  console.log(fun === result, fun);
}

Test(
  cartTotal(123),
  "Barang belanjaan M. Avdol adalah Susu Kambing, Telur Bebek, dan Tepung Kanji"
);
Test(
  cartTotal(1234),
  "Barang belanjaan J.P. Polnareff adalah Bunga Tulip Plastik, Makanan Kura-kura, Makanan Anjing, Permen Karet, dan Kaos Kaki Ajaib"
);

Test(
  cartTotal(010),
  "Barang belanjaan Kakyoin adalah Tupperware dan Jam Tangan"
);
