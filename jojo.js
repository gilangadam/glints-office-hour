//Terdapat dua buah array yang berisikan nama depan dan nama belakang delapan karakter utama dari Jojo's Bizzare Adventure.
// Buatlah fungsi bernama JoJo yang menerima parameter angka
// dimana angka itu adalah urutan dari tokoh utama dan menampilkan nama depan dan nama belakangnya secara benar.
// Apabila input diisi angka lebih dari 8, maka akan mereturn "Yo Araki, where's the next part at?"
// Input ->  JoJo(1), JoJo(9)
// Output -> Part 1 JoJo is Jonathan Joestar, Yo Araki, where's the next part at?

const firstName = [
  "Jonathan",
  "Joseph",
  "Jotaro",
  "Part 4 Josuke",
  "Giorno",
  "Jolyne",
  "Johnny",
  "Part 8 Josuke",
];
const lastName = [
  "Joestar",
  "Joestar",
  "Kujo",
  "Higashikata",
  "Giovanna",
  "Cujoh",
  "Joestar",
  "Higashikata",
];

const JoJo = (a) => {
 return a < 9 ? 
 `Part ${a} JoJo is ${firstName[a-1]} ${lastName[a-1]}` : 
 `Yo Araki, where's the next part at?`
};

//kode dibawah ini jangan diedit
function Test(fun, result) {
  console.log(fun === result, fun);
}
Test(JoJo(1), "Part 1 JoJo is Jonathan Joestar");
Test(JoJo(2), "Part 2 JoJo is Joseph Joestar");
Test(JoJo(3), "Part 3 JoJo is Jotaro Kujo");
Test(JoJo(4), "Part 4 JoJo is Part 4 Josuke Higashikata");
Test(JoJo(5), "Part 5 JoJo is Giorno Giovanna");
Test(JoJo(6), "Part 6 JoJo is Jolyne Cujoh");
Test(JoJo(7), "Part 7 JoJo is Johnny Joestar");
Test(JoJo(8), "Part 8 JoJo is Part 8 Josuke Higashikata");
Test(JoJo(9), "Yo Araki, where's the next part at?");
