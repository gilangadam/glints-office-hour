// Terdapat fungsi bernama Kurban yang menerima parameter berupa array. 
// Fungsi ini perlu di debug. Seharusnya fungsi ini mereturn perhitungan jumlah semua hewan kurban, 
// jumlah sapi, jumlah unta, jumlah kambing, dan jumlah domba.
// hint: pastikan tidak ada typo, gunakan console.log untuk melihat value setiap variable ,
// gunakan ctrl/cmd+click untuk melihat referensi variable, dan lihat hasil console untuk menentukan errornya dimana.
// Input -> Kurban(livestock)
// Output -> "Jumlah hewan kurban tahun ini adalah 11. Terdapat 3 sapi, 2 unta, 2 kambing, dan 4 domba"
const sapi = "sapi";
const unta = "unta";
const kambing = "kambing";
const domba = "domba";
  
const livestock = [
  sapi,
  unta,
  kambing,
  domba,
  sapi,
  domba,
  unta,
  kambing,
  sapi,
  domba,
  domba,
];

const Kurban = (arr) => {
  const jumlahSapi = arr.filter((x) => x === sapi).length;
  const jumlahUnta = arr.filter((x) => x === unta).length;
  const jumlahKambing = arr.filter((x) => x === kambing).length;
  const jumlahDomba = arr.filter((x) => x === domba).length;
  return `Jumlah hewan kurban tahun ini adalah ${
    jumlahUnta + jumlahSapi + jumlahKambing + jumlahDomba
  }. Terdapat ${jumlahSapi} sapi, ${jumlahUnta} unta, ${jumlahKambing} kambing, dan ${jumlahDomba} domba`;
};


//kode dibawah jangan diedit
function Test(fun, result) {
  console.log(fun === result, fun);
}

Test(
  Kurban(livestock),
  "Jumlah hewan kurban tahun ini adalah 11. Terdapat 3 sapi, 2 unta, 2 kambing, dan 4 domba"
);
Test(
  Kurban([sapi, kambing, sapi, kambing, domba, sapi, sapi, kambing]),
  "Jumlah hewan kurban tahun ini adalah 8. Terdapat 4 sapi, 0 unta, 3 kambing, dan 1 domba"
);
Test(
  Kurban([sapi, unta, kambing, domba]),
  "Jumlah hewan kurban tahun ini adalah 4. Terdapat 1 sapi, 1 unta, 1 kambing, dan 1 domba"
);
Test(
  Kurban([sapi, sapi, kambing, kambing, domba, unta, unta]),
  "Jumlah hewan kurban tahun ini adalah 7. Terdapat 2 sapi, 2 unta, 2 kambing, dan 1 domba"
);
